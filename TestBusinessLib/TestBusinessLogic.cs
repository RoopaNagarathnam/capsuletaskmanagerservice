﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TaskManagerBusinessLib;
using TaskManagerEntitiesLib;
namespace TestBusinessLib
{
    [TestFixture]
    public class TestBusinessLogic
    {
        [Test]
        public void TestGetAll()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            int actual = obj.GetAll().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestGetByTaskId()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = obj.GetById(4010);
            Assert.AreEqual(4010, item.TaskId);
        }
        [Test]
        public void TestAddTask()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = new Task();
            item.TaskName = "Task12";
            item.ParentTask = 4010;
            item.Priority = 15;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.Add(item);
            Task test = obj.GetByTaskName("Task12");            
            Assert.AreEqual("Task12", test.TaskName);

        }
        [Test]
        public void TestDeleteTask()
        {

            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.Delete(6024);
            Task item = obj.GetById(6024);
            Assert.AreEqual(null, item);
        }
        [Test]
        public void TestUpdateTask()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = new Task();
            item.TaskId = 4010;
            item.TaskName = "Test1";
            item.Priority = 18;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.Update(item);
            Task itemafterupdate = obj.GetByTaskName("Test1");
            Assert.AreEqual(18, itemafterupdate.Priority);           

        }
        [Test]
        public void TestUpdateEndTask()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            Task item = new Task();
            item.TaskId = 4010;
            item.TaskName = "Task1";
            item.Priority = 22;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.UpdateEndDate(item);
            Task itemafterupdate = obj.GetByTaskName("Task1");
            Assert.AreEqual(1, itemafterupdate.Flag);


        }
    }
}
