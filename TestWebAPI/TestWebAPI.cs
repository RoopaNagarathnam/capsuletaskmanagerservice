﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Web.Http;
using TaskManagerWebAPI.Controllers;
using TaskManagerEntitiesLib;
namespace TestWebAPI
{
    [TestFixture]
    public class TestWebAPI
    {
        [Test]
        public void TestWebAPIGetAll()
        {
            TaskManagerController obj = new TaskManagerController();
            List<Task> task =  obj.GetAll();
            Assert.Greater(task.Count(), 0);

        }
        [Test]
        public void TestWebAPIGetByTaskId()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = obj.GetById(4011);
            Assert.AreEqual(4011, item.TaskId);
        }
        //[Test]
        //public void TestWebAPIAddTask()
        //{
        //    TaskManagerController obj = new TaskManagerController();
        //    Task item = new Task();
        //    item.TaskName = "WebAPITask1";
        //    item.ParentTask = 2;
        //    item.Priority = 15;
        //    item.StartDate = System.DateTime.Now;
        //    item.EndDate = System.DateTime.Now;
        //    item.Flag = 0;
        //    IHttpActionResult ok = obj.PostTask(item);
        //    Task test = obj.GetByTaskName("WebAPITask1");
        //    Assert.AreEqual("WebAPITask1", test.TaskName);

        //}
        [Test]
        public void Testwebapideletetask()
        {

            TaskManagerController obj = new TaskManagerController();
            List<Task> task = obj.DeleteTask(6017);
            Assert.AreEqual(6, task.Count());
        }
        [Test]
        public void TestWebAPIUpdateTask()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = new Task();
            item.TaskId = 4010;
            item.TaskName = "Task1";
            item.Priority = 13;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.PutTask(item);
            Task itemafterupdate = obj.GetByTaskName("Task1");
            Assert.AreEqual(13, itemafterupdate.Priority);

        }
        [Test]
        public void TestWebAPIUpdateEndTask()
        {
            TaskManagerController obj = new TaskManagerController();
            Task item = new Task();
            item.TaskId = 6019;
            item.TaskName = "Task7";
            item.Priority = 22;
            item.StartDate = System.DateTime.Now;
            item.EndDate = System.DateTime.Now;
            item.Flag = 0;
            obj.UpdateEndDate(item);
            Task itemafterupdate = obj.GetByTaskName("Task7");
            Assert.AreEqual(1, itemafterupdate.Flag);


        }

    }
}
