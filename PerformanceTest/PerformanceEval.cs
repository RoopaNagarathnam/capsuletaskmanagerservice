﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using NBench.Util;
using TaskManagerBusinessLib;
using TaskManagerEntitiesLib;
namespace PerformanceTest
{
    public class PerformanceEval
    {

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
            NumberOfIterations = 1, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkAddUpdate()
        {

            Task task = new Task();
            task.TaskName = "test111";
            task.ParentTask = 4009;
            task.StartDate = System.DateTime.Now;
            task.EndDate = System.DateTime.Now;
            task.Flag = 0;
            task.Priority = 12;
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.Add(task);
            obj.Update(task);
            obj.UpdateEndDate(task);
            //obj.GetAll();
            //obj.GetByTaskName("test111");
            //obj.GetById(8051);
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
            NumberOfIterations = 1, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkView()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.GetAll();
            //obj.GetByTaskName("test111");
            //obj.GetById(8051);
        }
        
        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
          NumberOfIterations = 1, RunMode = RunMode.Throughput,
          RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkGetByTaskId()
        {
            TaskManagerBusiness obj = new TaskManagerBusiness();
            obj.GetById(8051);
        }

        [PerfCleanup]
        public void Cleanup()
        {
            // does nothing
        }

    }
}
