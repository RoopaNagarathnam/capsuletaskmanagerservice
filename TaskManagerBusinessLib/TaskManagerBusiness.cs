﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TaskManagerDataLib;
using TaskManagerEntitiesLib;

namespace TaskManagerBusinessLib
{
    public class TaskManagerBusiness
    {
        public void Add(Task item)
        {                   
         
            using (TaskContext dbcontext = new TaskContext())
            {
                dbcontext.Tasks.Add(item);
                dbcontext.SaveChanges();
            }
        }
        public List<Task> Delete(int id)
        {
            using (var ctx = new TaskContext())
            {
                var task = ctx.Tasks
                    .Where(s => s.TaskId == id)
                    .FirstOrDefault();

                ctx.Entry(task).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Tasks.ToList();
             }
          
        }
        public List<Task> UpdateEndDate(Task item)
        {
            using (TaskContext dbcontext = new TaskContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskId == item.TaskId);
                context.EndDate = System.DateTime.Now;
                context.Flag = 1;
                dbcontext.SaveChanges();
                return dbcontext.Tasks.ToList();
                
            }

        }
        public void Update(Task item)
        {
            using (TaskContext dbcontext = new TaskContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskId == item.TaskId);
                context.TaskId = item.TaskId;
                context.TaskName = item.TaskName;
                context.ParentTask = item.ParentTask;
                context.Priority = item.Priority;
                context.StartDate = item.StartDate;
                context.EndDate = item.EndDate;
                dbcontext.SaveChanges();

            }

        }
        public List<Task> GetAll()
        {

            using (TaskContext dbcontext = new TaskContext())
            {

                return dbcontext.Tasks.ToList();
            }
        }
        public Task GetByTaskName(string TaskName)
        {
            
                using (TaskContext dbcontext = new TaskContext())
                {
                    var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskName == TaskName);
                    return context;
                }
           
        }
        public Task GetById(int TaskId)
        {

            using (TaskContext dbcontext = new TaskContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskId == TaskId);
                return context;
            }

        }


    }
}
