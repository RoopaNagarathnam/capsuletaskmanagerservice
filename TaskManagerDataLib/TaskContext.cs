﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TaskManagerEntitiesLib;

namespace TaskManagerDataLib
{
    public class TaskContext:DbContext
    {
        public TaskContext() : base("name = DBConnString")
        {
        }
        public DbSet<Task> Tasks { get; set; }
    }
}
