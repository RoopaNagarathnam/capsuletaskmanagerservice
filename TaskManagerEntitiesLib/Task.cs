﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagerEntitiesLib
{

    public class Task
    {
        [Key]
        public int TaskId { get; set; }

        public int ParentTask { get; set; }
        public string TaskName { get; set; }
        public int Priority { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        
        public DateTime EndDate { get; set; }

        public string StartDateF
        {

            get
            {

              return  StartDate.ToString("yyyy-MM-dd");
            }
            
        }
        public string EndDateF
        {

            get
            {

                return EndDate.ToString("yyyy-MM-dd");
            }

        }

        public int Flag { get; set; }

    }


}
